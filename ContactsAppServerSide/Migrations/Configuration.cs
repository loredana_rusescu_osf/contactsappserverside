using System.Data.Entity.Migrations;
using ContactsAppServerSide.Models;

namespace ContactsAppServerSide.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ContactServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ContactServiceContext context)
        {
            context.Contacts.AddOrUpdate(
              c => c.Id,
              new Contact { FirstName = "Andrei", LastName = "Popescu", Address = "Str. Baicului nr 49", TelephoneNumber = "0723002908" },
              new Contact { FirstName = "Elvira", LastName = "Ionescu", Address = "Str. Andries nr 3", TelephoneNumber = "0736002109" },
              new Contact { FirstName = "Crina", LastName = "Maftei", Address = "Str. Polona nr 12", TelephoneNumber = "0749807503" }
            );

        }
    }
}

