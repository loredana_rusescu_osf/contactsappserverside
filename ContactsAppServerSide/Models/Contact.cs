﻿using System.ComponentModel.DataAnnotations;

namespace ContactsAppServerSide.Models
{
    public class Contact
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }

        [Required]
        public string TelephoneNumber { get; set; }

        public string Image { get; set; }
    }
}